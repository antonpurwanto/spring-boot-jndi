package com.example.demo.jndi;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jndi.utilities.TraceLog;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration(exclude = { HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class,
		DataSourceAutoConfiguration.class })
public class BootDemoJndiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootDemoJndiApplication.class, args);
	}

	@Bean
	public TomcatEmbeddedServletContainerFactory tomcatFactory() { // if datasouce defind in tomcat xml configuration
																	// then no need to create this bean
		return new TomcatEmbeddedServletContainerFactory() {
			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override // create JNDI resource
			protected void postProcessContext(Context context) {

				ContextResource resource = new ContextResource();
				resource.setName("jndiDataSource");
				TraceLog.info(this.getClass(), resource.getName());
				resource.setType(DataSource.class.getName());
				resource.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
				resource.setProperty("url", "jdbc:mysql://localhost:3306/test");
				resource.setProperty("username", "root");
				resource.setProperty("password", "java");
				context.getNamingResources().addResource(resource);

				ContextResource resource1 = new ContextResource();
				resource1.setName("jdbc/jndiDataSource");
				TraceLog.info(this.getClass(), resource1.getName());
				resource1.setType(DataSource.class.getName());
				resource1.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
				resource1.setProperty("url", "jdbc:mysql://localhost:3306/coba?useSSL=false");
				resource1.setProperty("username", "root");
				resource1.setProperty("password", "java");
				context.getNamingResources().addResource(resource1);

			}
		};
	}

	@Bean(destroyMethod = "")
	public DataSource jndiDataSource() throws IllegalArgumentException, NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean(); // create JNDI data source
		bean.setJndiName("java:/comp/env/jndiDataSource"); // jndiDataSource is name of JNDI data source
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		return (DataSource) bean.getObject();
	}

	@Bean
	@Qualifier("jndiTemplate")
	public JdbcTemplate jndiTemplate() throws IllegalArgumentException, NamingException {
		return new JdbcTemplate(jndiDataSource());
	}

	@Bean(destroyMethod = "")
	public DataSource jdbcJndiDataSource() throws IllegalArgumentException, NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean(); // create JNDI data source
		bean.setJndiName("java:/comp/env/jdbc/jndiDataSource"); // jndiDataSource is name of JNDI data source
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		return (DataSource) bean.getObject();
	}

	@Bean
	@Qualifier("jdbcJndiTemplate")
	public JdbcTemplate jdbcJndiTemplate() throws IllegalArgumentException, NamingException {
		return new JdbcTemplate(jdbcJndiDataSource());
	}
}

@RestController
class EmployeeController {

	@Autowired
	private OrangDAO orangDao;

	@Autowired
	private AlamatDAO alamatDao;

	@RequestMapping("/getOrangList")
	public List getDaftarOrang() {
		return orangDao.getEmployeeList();
	}

	@RequestMapping("/getAlamatList")
	public List getDaftarAlamat() {
		return alamatDao.getEmployeeList();
	}
}

@Component
class OrangDAO {

	@Autowired
	@Qualifier("jndiTemplate")
	private JdbcTemplate jndiTemplate;

	public List<Orang> getEmployeeList() {

		List<Map<String, Object>> p = jndiTemplate.queryForList("select * from Orang");

		return p.stream().map(e -> {
			Orang orang = new Orang();
			orang.setId((String) e.get("id"));
			orang.setNama((String) e.get("nama"));
			return orang;
		}).collect(Collectors.toList());

	}
}

@Component
class AlamatDAO {

	@Autowired
	@Qualifier("jdbcJndiTemplate")
	private JdbcTemplate jdbcJndiTemplate;

	public List<Alamat> getEmployeeList() {

		List<Map<String, Object>> adress = jdbcJndiTemplate.queryForList("select * from Latihan");

		return adress.stream().map(e -> {
			Alamat alamat = new Alamat();
			alamat.setEmail((String) e.get("email"));
			return alamat;
		}).collect(Collectors.toList());

	}
}

class Orang {

	private String id;
	private String nama;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

}

class Alamat {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Alamat email = " + email;
	}

}